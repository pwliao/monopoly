#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include "conio.h"
#include "Land.h"
#include "Player.h"

using namespace std;

extern const int initMoney = 200;
extern const int N = 28;
extern const int px[] = {1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101, 111, 111, 111, 111, 101, 91, 81, 71, 61, 51, 41, 31, 21, 11, 1, 1, 1};
extern const int py[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 15, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 15, 8};

void init(Player *);
void draw(int x, int y);
void drawboard();
void rule();
void check(Player *);

int main()
{
	setvbuf(stdout, NULL, _IONBF, 0);
	Player player[4];
	init(player);
	rule();
	drawboard();
	srand(time(NULL));
	int c, curplayer = 0;
	player[0].printMoney(true);
	while ((c = getch()) != 27) {
		if (c != ' ') continue;
		player[curplayer].move(player);
		for (int i = 0; i < 4; i++)
			player[i].printMoney(false);
		curplayer = (curplayer + 1) % 4;
		while (player[curplayer].getMoney() < 0) {
			curplayer = (curplayer + 1) % 4;
		}
		player[curplayer].printMoney(true);
		check(player);
	}
	gotoxy(121, 28);
	return 0;
}

void init(Player *player)
{
	printf("\033[?25l"); // hide the cursor
	system("clear");
	printf("\033[0m");
	// print start screen
	ifstream in("art.txt");
	string s;
	while (getline(in, s)) {
		for (int i = 0; i < 20; i++)
			cout.put(' ');
		cout << s << endl;
	}
	in.close();
	for (int i = 0; i < 4; i++)
		player[i].setNum(i + 1);
	Player::initLand();
	getch();
}
// draw a block
void draw(int x, int y)
{
	int W = 10, H = 5, x1 = x;
	gotoxy(x, y);
	for (int i = 0; i < W; i++)
		putchar('-');
	gotoxy(x, ++y);
	for (int i = 0; i < H; i++) {
		for (int j = 0; j < W; j++) {
			if (j == 0 || j == W - 1)
				putchar('|');
			gotoxy(++x, y);
		}
		x = x1;
		gotoxy(x, ++y);
	}
	for (int i = 0; i < W; i++)
		putchar('-');
}

void drawboard()
{
	system("clear");
	for (int i = 0; i < N; i++) {
		draw(px[i], py[i]);
	}
	FILE *f = fopen("board.txt", "r");
	for (int i = 0; i < N; i++) {
		char a[15], b[15];
		fscanf(f, "%s%s", a, b);
		gotoxy(50, 15);
		if (a[0] == '0') continue;
		gotoxy(px[i] + 1, py[i] + 1);
		if (strlen(a) == 6) printf("  ");
		else if (strlen(a) == 9) putchar(' ');
		printf("%s", a);
		if (b[0] == '0') continue;
		gotoxy(px[i] + 1, py[i] + 5);
		for (int i = (7 - strlen(b)) / 2; i > 0; i--)
			putchar(' ');
		printf("%s", b);
		if (strlen(b) < 8) printf("W");
	}
	fclose(f);
	// Player
	gotoxy(2, 3);
	printf("A B C D");
	for (int i = 0; i < 4; i++) {
		gotoxy(12, 9 + i);
		printf("\033[1;%dm", 32 + i);
		printf("%c: %dW", 'A' + i, initMoney);
	}
	printf("\033[0m");
	// Dice
	gotoxy(102, 16);
	printf("-----");
	gotoxy(102, 17);
	printf("|   |");
	gotoxy(102, 18);
	printf("| ● |");
	gotoxy(102, 19);
	printf("|   |");
	gotoxy(102, 20);
	printf("-----");
}

// print rule
void rule()
{
	system("clear");
	gotoxy(40, 10);
	ifstream in("rule.txt");
	string s;
	cout << endl;
	while (getline(in, s)) {
		for (int i = 0; i < 40; i++)
			cout.put(' ');
		cout << s << endl;
	}
	in.close();
	char c;
	do {
		c = getch();
		c = tolower(c);
	} while (c != 's' && c != 'r');
	if (c == 'r') {
		system("clear");
		gotoxy(45, 1);
		cout << endl;
		ifstream ru("page.txt");
		string str;
		while (getline(ru, str)) {
			for (int i = 0; i < 15; i++)
				cout.put(' ');
			cout << str << endl;
		}
		gotoxy(80, 24);
		cout << "┌-------------┐" << endl;
		gotoxy(80, 25);
		cout << "|   START(s)  |" << endl;
		gotoxy(80, 26);
		cout << "└-------------┘" << endl;
		char ch;
		do {
			ch = getch();
			ch = tolower(ch);
		} while (ch != 's');
		ru.close();
	}
}

// check who wins
void check(Player *player)
{
	int alive = 0, n = 0;
	for (int i = 0; i < 4; i++)
		if (player[i].getMoney() >= 0)
			alive++, n = i;
	if (alive == 1) {
		ifstream in;
		if (n == 0) in.open("winnerA.txt");
		else if (n == 1) in.open("winnerB.txt");
		else if (n == 2) in.open("winnerC.txt");
		else in.open("winnerD.txt");
		system("clear");
		string s;
		cout << endl << endl;
		while (getline(in, s)) {
			for (int i = 0; i < 30; i++)
				cout.put(' ');
			cout << s << endl;
		}
		getch();
		gotoxy(1, 28);
		in.close();
		exit(0);
	}
}
