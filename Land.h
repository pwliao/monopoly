#ifndef LAND_H
#define LAND_H
class Waterpower
{
public:
	Waterpower();
	void show();
	bool showBuy(int);
	int getOwner() const; // return owner
	int getPrice() const; // return price
	void setPrice(int p);
	void setName(const char *);
	char *getName() {return name;}
	void reset(){price = 80, owner = -1;}
protected:
	char name[15];
	int price;
	int owner; // owner 1 ~ 4
};
class Highway: public Waterpower
{
public:
	Highway() {price = 50, owner = -1, rent = 10;}
	void reset() {price = 50, owner = -1;}
	int getRent() const {return rent;}
private:
	int rent;
};
class Land: public Waterpower
{
public:
	Land();
	void setRent(int *);
	void setBuildMoney(int);
	int getRent() const;
	int showBuild(int);
	void show();
	int getRentshow(int);
	void setAcom(int n);
	int getAcom();
	void reset();
	void showInform();
	int getBuildmoney() const;
private:
	int house; // number of house 0 ~ 3
	int rent[5];
	int buildmoney;
	int acom;
};

#endif
