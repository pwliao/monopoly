CFLAGS =  -O2 -Wall -Wno-unused-result
CC = g++

all: monopoly

fast: CFLAGS += -DFAST
fast: monopoly

monopoly: main.o Player.o Land.o conio.o
	$(CC) main.o Player.o Land.o conio.o -o monopoly $(CFLAGS)

main.o: main.cpp Player.h Land.h conio.h
	$(CC) -c main.cpp $(CFLAGS)
Player.o: Player.cpp Player.h Land.h conio.h
	$(CC) -c Player.cpp $(CFLAGS)
Land.o: Land.cpp Land.h conio.h
	$(CC) -c Land.cpp $(CFLAGS)
conio.o: conio.cpp
	$(CC) -c conio.cpp $(CFLAGS)

run: monopoly
	./monopoly

clean:
	rm -f main.o Player.o Land.o conio.o monopoly
