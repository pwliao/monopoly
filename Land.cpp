#include "Land.h"
#include "conio.h"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <string>
using namespace std;
extern const int px[], py[];
Waterpower::Waterpower()
{
	price = 80;
	owner = -1;	
}
int Waterpower::getOwner() const
{
	return owner;
}
int Waterpower::getPrice() const
{
	return price;
}
void Waterpower::setPrice(int p)
{
	price = p;
}
Land::Land()
{
	owner = -1;
	house = 0;
}
void Waterpower::setName(const char *c)
{
	strcpy(name, c);
}
void Waterpower::show()
{
	gotoxy(31, 9);
	for (int i = 0; i < 15; i++)
		putchar(' ');
	gotoxy(31, 9);
	cout << name << "  " << price << "w";
}
void Land::show()
{
	Waterpower::show();
	showInform();
}
bool Waterpower::showBuy(int num)
{
	char buy;
	gotoxy(31, 18);
	printf("buy?(Y for yes ; N for no) ");
	do {
		buy = getch();
		buy = toupper(buy);
	} while (buy != 'Y' && buy != 'N');
	if (buy == 'Y') {
		owner = num;
	}
	return ~owner; // return owner != -1
}
int Land::showBuild(int pos)
{
	showInform();
	gotoxy(31, 18);
	char s;
	int moneydec = 0;
	if (house < 3) {
		printf("build house?(Y for yes ; N for no) ");
		do {
			s = getch();
			s = toupper(s);
		} while (s != 'Y' && s != 'N');
		if (s == 'Y') {
			house++;
			moneydec = buildmoney;
			gotoxy(px[pos] + house * 2 - 1, py[pos] + 4);
			printf("\033[1;32m");  // red
			putchar('H');
			printf("\033[0m");
		}
	}
	gotoxy(31, 18);
	for (int i = 0; i < 40; i++)
		putchar(' ');
	for (int y = 11; y <= 16; y++) {
		gotoxy(31, y);
		for (int i = 0; i < 40; i++)
			putchar(' ');
	}
	gotoxy(31, 9);
	for (int i = 0; i < 15; i++)
		putchar(' ');
	return moneydec; // money need to pay
}
// show land information
void Land::showInform()
{
	gotoxy(31, 11);
	cout << "過路費：" << getRentshow(0) << "w" << endl;
	gotoxy(31, 12);
	cout << "一棟房子過路費：" << getRentshow(1) << "w" << endl;
	gotoxy(31, 13);
	cout << "兩棟房子過路費：" << getRentshow(2) << "w" << endl;
	gotoxy(31, 14);
	cout << "三棟房子過路費：" << getRentshow(3) << "w" << endl;
	gotoxy(31, 15);
	cout << "蓋房子：" << buildmoney << "w" << endl;
	gotoxy(31, 16);
	cout << "住宿費(一晚): " << getAcom() << "w" << endl;
}
void Land::setRent(int *a)
{
	for (int i = 0; i < 4; i++)
		rent[i] = a[i];
}
int Land::getAcom()
{
	return acom;
}
void Land::setBuildMoney(int n)
{
	buildmoney = n;
}
int Land::getRent() const
{
	return rent[house];
}
int Land::getRentshow(int n)
{
	return rent[n];
}
void Land::reset()
{
	owner = -1;
	house = 0;
}
int Land::getBuildmoney() const
{
	return buildmoney;
}
void Land::setAcom(int n)
{
	acom = n;
}
