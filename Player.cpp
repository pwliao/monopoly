#include "Player.h"
#include "Land.h"
#include "conio.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <unistd.h>
#include <iostream>
using namespace std;
extern int initMoney, N, px[], py[];
int Player::l[28] = { -1, 0, 1, 2, 3, -1, 4, 5, 6, 7, 8, -1, -1, 9, -1, 10, 11, 12, 13, -1, 14, 15, 16, 17, 18, -1, -1, 19};
int Player::landpos[20] = {1, 2, 3, 4, 6, 7, 8, 9, 10, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24, 27};
Land Player::land[20] = {};
Highway Player::highway = Highway();
Waterpower Player::waterpower = Waterpower();
void draw(int x, int y);
Player::Player()
{
	money = initMoney;
	position = 0;
	pause = 0;
	prisonout = 0;
	gold = 0;
}
void Player::setNum(int n)
{
	num = n;
}
int Player::getMoney() const
{
	return money;
}
int Player::getGold() const
{
	return gold;
}
void Player::move(Player *player)
{
	if (pause) {
		pause--;
		return;
	}
#ifndef FAST
	sleep(1);
#endif
	int step = rollDice();
	if (position + step > N) money += 20;
	for (int i = 0; i < step; i++) {
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar(' ');
		position = (position + 1) % N;
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar('A' + num - 1);
		fflush(stdout);
#ifndef FAST
		usleep(500000);
#endif
	}
	int lpos = l[position];
	if (lpos != -1) {
		land[lpos].show();
		int owner = land[lpos].getOwner();
		if (owner == -1) {
			if (land[lpos].getPrice() > money) { // doesn't have enough money
                gotoxy(31,18);
                cout << "Sorry, you can't buy!";
				getch();
                gotoxy(31,18);
                for(int i=0;i<30;i++)
                    putchar(' ');
			} else if (land[lpos].showBuy(num)) {
				money -= land[lpos].getPrice();
				printBuy(); // print color on the land
			}
		} else if (owner == num) {
			int buildmoney = land[lpos].getBuildmoney() * 6 / 5;
			if (buildmoney > money ) {
				getch();
			} else {
				int moneydec = land[lpos].showBuild(position);
				money -= moneydec;
				money -= moneydec / 5;
				int owner = waterpower.getOwner();
				if (owner != -1) {
					player[owner - 1].addMoney(moneydec / 5);
				}
			}
		} else {
			payLand(player);
		}
	} else if (position == 5 || position == 19) { // chance
		chance(player);
	} else if (position == 12 || position == 26) { // destiny
		destiny(player);
	} else if (position == 11) { // highway
		highway.show();
		int owner = highway.getOwner();
		int rent = highway.getRent();
		if (owner != -1 && owner != num) {
			gotoxy(31, 19);
			printf("You have to pay %d", rent);
			printf(" to %c", highway.getOwner() + 'A' - 1);
			if (money < rent) { // bankrupt
				player[owner - 1].addMoney(money);
				money = -1;
				bankrupt(); // print color on the land
			} else { // pay rent
				money -= rent;
				player[owner - 1].addMoney(rent);
			}
			getch();
		} else if (owner != num) {
			if (50 <= money && highway.showBuy(num)) {
				money -= 50;
				printBuy(); // print color on the land
			}
		}
	} else if (position == 14) { // waterpower
		gotoxy(31, 9);
		if (waterpower.getOwner() == -1 && 80 <= money && waterpower.showBuy(num)) {
			money -= 80;
			printBuy(); // print color on the land
		}
	} else if (position == 25) {
		pause++;
		prison();
	}
	clearMsg();
}
void Player::payLand(Player *player)
{
	int lpos = l[position];
	gotoxy(31, 10);
	cout << "所有者:" << char(land[lpos].getOwner() + 64);
	land[lpos].showInform();
	gotoxy(31, 18);
	cout << "Pay or Stay ?(P for Pay ; S for Stay)" << endl;
	char pay;
	do {
		pay = getch();
		pay = toupper(pay);
	} while (pay != 'P' && pay != 'S');
	if (pay == 'P') {
		gotoxy(31, 19);
		printf("You have to pay %d", land[lpos].getRent());
		printf(" to %c", land[lpos].getOwner() + 'A' - 1);
		int rent = land[lpos].getRent();
		if (money >= rent) {
			money -= rent;
			player[land[lpos].getOwner() - 1].addMoney(land[lpos].getRent());
		} else {
			player[land[lpos].getOwner() - 1].addMoney(money);
			money = -1;
			bankrupt();
		}
	} else if (pay == 'S') {
		int acom = land[lpos].getAcom();
		pause += 1;
		gotoxy(31, 19);
		cout << "You can stay here and you have to pay " << acom << "w" << endl;
		if (money >= acom) {
			money -= acom;
			player[land[lpos].getOwner() - 1].addMoney(land[lpos].getAcom());
		} else {
			player[land[lpos].getOwner() - 1].addMoney(money);
			money = -1;
			bankrupt();
		}
	}
	getch();
	clearMsg();
}
// add money
void Player::addMoney(int n)
{
	money += n;
}
// roll dice return step
int Player::rollDice()
{
	int n = 15, step;
	while (n--) {
		step = rand() % 6 + 1;
		for (int i = 0; i < 3; i++) {
			gotoxy(103, 17 + i);
			printf("   ");
		}
		if (step & 1) {
			gotoxy(104, 18);
			printf("●");
		}
		if (step >= 2 ) {
			gotoxy(103, 17);
			printf("●");
			gotoxy(105, 19);
			printf("●");
			if (step >= 4) {
				gotoxy(105, 17);
				printf("●");
				gotoxy(103, 19);
				printf("●");
			}
			if (step == 6) {
				gotoxy(103, 18);
				printf("●");
				gotoxy(105, 18);
				printf("●");
			}
		}
		gotoxy(121, 28);
		fflush(stdout);
#ifndef FAST
		usleep(50000); // delay
#endif
	}
	return step;
}
void Player::printMoney(bool highlight) const
{
	gotoxy(12, 8 + num);
	for (int i = 0; i < 10; i++)
		putchar(' ');
	if (highlight) // reverse color
		printf("\033[7;%dm", 31 + num);
	else
		printf("\033[1;%dm", 31 + num);
	gotoxy(12, 8 + num);
	if (money >= 0) // print money
		printf("%c: %dW", 'A' + num - 1, money);
	else  // bankruptcy
		printf("%c: bankruptcy", 'A' + num - 1);
	printf("\033[1;0m");
}
void Player::initLand()
{
	// read board
	FILE *f = fopen("board.txt", "r");
	char a[15], b[15];
	int i = 0;
	while (fscanf(f, "%s%s", a, b) != EOF) {
		if (a[0] == '0' || b[0] <= '0' || b[0] > '9') continue;
		land[i].setName(a);
		land[i].setPrice(atoi(b));
		i++;
	}
	fclose(f);
	// read land information
	f = fopen("landInfo.txt", "r");
	int c[6];
	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 6; j++)
			fscanf(f, "%d", &c[j]);
		land[i].setRent(c);
		land[i].setBuildMoney(c[4]);
		land[i].setAcom(c[5]);
	}
	fclose(f);
	waterpower.setName("水電公司");
	highway.setName("高速公路");
}
void Player::chance(Player *player)
{
	int card;
	srand(time(NULL));
	card = rand() % 8;
	if (card == 0) {
		gotoxy(31, 12);
		cout << "年終獎金得50萬元";
		money = money + 50; //get extra money 50W
	} else if (card == 1) {
		gotoxy(31, 12);
		cout << "捐錢20萬元";
		money = money - 20; //donate 20W
		if (money < 0) bankrupt();
	} else if (card == 2) {
		gotoxy(31, 12);
		cout << "再擲一次骰子,若是奇數點則得到所擲點數十倍的金額,若偶數點則罰款其十倍金額";
		getch();
		int dice = rollDice();
		if (dice % 2 == 1) {
			money += dice * 10;
		} else {
			money -= dice * 10;
			if (money < 0) bankrupt();
		}
	} else if (card == 3) {
		gotoxy(31, 12);
		cout << "出獄許可證";
		//cout<<"player加上出獄許可證變數";
		prisonout++;
	} else if (card == 4) {
		gotoxy(31, 12);
		cout << "直接到'開始'處";
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar(' ');
		position = 0;
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar('A' + num - 1);
	} else if (card == 5) {
		gotoxy(31, 12);
		cout << "中樂透搜括下一個人一半的錢";
		int n = (num) % 4;
		while (player[n].getMoney() < 0) {
			n = (n + 1) % 4;
		}
		int m = player[n].getMoney();
		if (player[n].getGold()) {
			gold--;
		} else {
			money += m / 2;
			player[n].addMoney(-m / 2);
		}
	} else if (card == 6) {
		gotoxy(31, 12);
		cout << "繳罰金20W或是抽一張命運卡";
		gotoxy(31, 13);
		cout << "輸入(a)罰款輸入(b)抽命運卡";
		char c;
		do {
			c = getch();
			c = tolower(c);
		} while (c != 'a' && c != 'b');
		if (c == 'a') {
			money = money - 20;
			clearMsg();
			if (money < 0) bankrupt();
			return;
		} else {
			clearMsg();
			destiny(player);
			return;
		}
	} else if (card == 7) {
		gotoxy(31, 12);
		cout << "賣黃牛票獲利40W";
		money = money + 40;
	}
	getch();
	clearMsg();
}
void Player::destiny(Player *player)
{
	int card;
	srand(time(NULL));
	card = rand() % 9;
	if (card == 0) {
		gotoxy(31, 12);
		cout << "壽星可向每人索取20W";
		for (int i = 0; i < 4; i++) {
			if (i == num - 1) continue;
			int m = player[i].getMoney();
			if (m < 0) continue;
			if (m < 20) {
				player[i].addMoney(-m - 1);
				player[i].bankrupt();
				money += m;
			} else {
				player[i].addMoney(-20);
				money += 20;
			}
		}
	} else if (card == 1) {
		gotoxy(31, 12);
		cout << "立即坐牢";
		pause++;
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar(' ');
		position = 25;
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar('A' + num - 1);
		prison();
	} else if (card == 2) {
		gotoxy(31, 12);
		cout << "典當金飾得50W";
		money = money + 50;
	} else if (card == 3) {
		gotoxy(31, 12);
		cout << "免死金牌乙張,若有人中樂透可不被搜刮";
		gold++;
	} else if (card == 4) {
		gotoxy(31, 12);
		cout << "再擲一次骰子,若是奇數點則得到所擲點數十倍的金額,若偶數點則罰款其十倍金額";
		getch();
		int dice = rollDice();
		if (dice % 2 == 1) {
			money += dice * 10;
		} else {
			money -= dice * 10;
			if (money < 0) bankrupt();
		}
	} else if (card == 5) {
		gotoxy(31, 12);
		cout << "賣黃牛票被抓罰款30W";
		money = money - 30;
		if (money < 0) bankrupt();
	} else if (card == 6) {
		gotoxy(31, 12);
		cout << "拘票,繳罰款20W或是暫停一次";
		gotoxy(31, 13);
		cout << "輸入(a)罰款輸入(b)暫停一次";
		char c;
		do {
			c = getch();
			c = tolower(c);
		} while (c != 'a' && c != 'b');
		if (c == 'a') {
			money = money - 20;
			if (money < 0) bankrupt();
		} else
			pause++;
		clearMsg();
		return;
	} else if (card == 7) {
		gotoxy(31, 12);
		cout << "繳罰金20W或是抽一張命機會卡";
		gotoxy(31, 13);
		cout << "輸入(a)罰款輸入(b)抽機會卡";
		char c;
		do {
			c = getch();
			c = tolower(c);
		} while (c != 'a' && c != 'b');
		if (c == 'a') {
			money = money - 20;
			clearMsg();
			if (money < 0) bankrupt();
			return;
		} else {
			clearMsg();
			chance(player);
			return;
		}
	} else if (card == 8) {
		int a[20];
		int count = 0;
		for (int i = 0; i < 20; i++) {
			if (land[i].getOwner() == num) {
				a[count] = i;
				count++;
			}
		}
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar(' ');
		if (count > 0) {
			int h = rand() % count;
			position = landpos[a[h]];
		} else {
			position = 0;
		}
		gotoxy(31, 12);
		cout <<"當地主兩回合(若沒土地則到開始)";
		gotoxy(px[position] + num * 2 - 1, py[position] + 2);
		putchar('A' + num - 1);
		pause += 2;
	}
	getch();
	clearMsg();
}
// clear message
void clearMsg()
{
	for (int y = 9; y <= 15; y++) {
		gotoxy(31, y);
		for (int i = 0; i < 75; i++)
			putchar(' ');
	}
	for (int y = 16; y <= 19; y++) {
		gotoxy(31, y);
		for (int i = 0; i < 60; i++)
			putchar(' ');
	}
}
void Player::bankrupt()
{
	for (int i = 0; i < 20; i++) {
		if (land[i].getOwner() == num) {
			land[i].reset();
			gotoxy(px[landpos[i]] + 1, py[landpos[i]] + 4);
			cout << "        ";
			draw(px[landpos[i]], py[landpos[i]]);
		}
		if (highway.getOwner() == num) {
			highway.reset();
			draw(px[11], py[11]);
		}
		if (waterpower.getOwner() == num) {
			waterpower.reset();
			draw(px[14], py[14]);
		}
	}
}
void Player::printBuy()
{
	printf("\033[1;%dm", 31 + num);
	draw(px[position], py[position]);
	printf("\033[0m");
}
void Player::prison()
{
	if (prisonout == 0) return;
	gotoxy(31, 18);
	printf("使用出獄許可證?(Y for yes ; N for no) ");
	char c;
	do {
		c = getch();
		c = toupper(c);
	} while (c != 'Y' && c != 'N');
	if (c == 'Y') {
		prisonout--;
		pause--;
	}
}
