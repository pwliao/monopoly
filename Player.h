#ifndef PLAYER_H
#define PLAYER_H
#include "Land.h"
class Player
{
public:
	Player();
	void move(Player *);
	void setNum(int);
	void printMoney(bool) const;
	void addMoney(int);
	static void initLand();
	void payLand(Player *);
	void chance(Player *);
	void destiny(Player *);
	int getMoney() const;
	int getGold() const;
	void bankrupt();
	void printBuy();
	void prison();
private:
	int num;  // 1 ~ 4
	int money;
	int position;
	int pause;
	int rollDice();
	static Land land[20];
	static Highway highway;
	static Waterpower waterpower;
	int prisonout;
	int gold;
	static int l[28];
	static int landpos[20];
};
void clearMsg();
#endif
